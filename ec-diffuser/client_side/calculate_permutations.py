from math import floor
import copy

sep = ['-',  # Seperator for name-value pair
       '_',  # Sperator for pair1_pair2
       'm']  # Notation for negitive value

def create_empty_ndim_list(size = [1]):
  compiled_list = list([1] * size[len(size)-1])         # Initial size is the number of elements (last index)
  for i in range(len(size)-2, -1, -1):                  # Then for each parent tree
    sub_compiled  = compiled_list                       # We store the subtree (what we have up to now)
    compiled_list = list()                              # Make the parent tree
    for i in range(0, size[i]):                         # And then for the width of the tree
      compiled_list.append(copy.deepcopy(sub_compiled)) # we append i copies of the subtree
  print(compiled_list)
  return compiled_list

def set_ndim_element(ndim_list, position, value, level = 0):
  if type(ndim_list[position[level]]) is list:          # If we are still in a parent list
    set_ndim_element(ndim_list[position[level]],        # then go into the sublist at the position corrosponding to this level taking this level
                     position,                          # copying the position matrix
                     value,                             # value we are setting the matrix[position] to
                     level+1)                           # and an indication of how far we are in the search
  else:                                                 # If at a value
    print("Setting Matrix{} to {}".format(              # Indicate that we're setting that value
      position, value))   
    ndim_list[position[level]] = value                  # And set the value
    

def encode_list_1D(study, values):                      # 1D encode is super simple
  compiled_list = list()                                # We create a list
  for value in values[0]:                               # and for each variable
    if value[0] == '-':                                 # ensuring the - sign is
        value = 'm'+str(value[1:])                      # replaced with a m
    compiled_list.append(study[0] + sep[0] + value)     # we add the value to the list
    
  for dim in compiled_list:
    print(dim)

def encode_list (study, values):                        # ndim is not nearly as easy
  if len(study) != len(values):                         # first check for correct sizing
    raise RuntimeError("Study and Values arrays must have same number of dimensions")
  
  if len(study) == 1:                                   # then if given a 1D array
    return encode_list_1D(study, values)

  # First we calculate the number of unique combinations
  number_combinations = 1                                  # With the Number of combinations set to
  for value in values:                                     # a value that is
    number_combinations = number_combinations * len(value) # the product of the length of all values

  # Create a ndim matrix for all the varied dimensions
  size = [len(values[i]) for i in range(0, len(values))]       # We grab the size of each dimension
  compiled_list = create_empty_ndim_list(size)                 # and create an empty ndim list

  # Create the variation matrix
  for parent_level in range(1, number_combinations+1):         # For every combination
    compiled_sublist = list([0]*len(values))                   # create a sublist that represents the tree location

    # Each variation is a unique combination of each variable though a tree
    for i in range(len(values)-1, 0, -1):                                 # For each element in the combination
      this_tree    = (parent_level-1)/float(len(values[i])) + 1           # Determine on which tree we are on
      parent_level = floor(this_tree)                                     # The parent tree is the integer part
      this_level   = round((this_tree - parent_level)*len(values[i])) + 1 # The fractional part x detail level is the current tree item
      compiled_sublist[i] = this_level -1                                 # Set this level
      compiled_sublist[i-1] = parent_level-1                              # Set parent level

    # Compile name
    case_name = ""
    
    for level in range(0, len(compiled_sublist)):          # The case name is a combination
      value = values[level][compiled_sublist[level]]
      if '-' in str(value):
        value = sep[2]+str(value)
      case_name = (case_name    + sep[1] +                 # [case_name]_
                   study[level] + sep[0] +                 # [variable name]-
                   values[level][compiled_sublist[level]]) # [variable value]

    set_ndim_element(compiled_list, compiled_sublist, case_name[1:])
    
  for dim in compiled_list:
    print(dim)
  

                      
if __name__ == "__main__":
  study      = ['A', 'B', 'C']; values = [['a', 'b', 'c'], ['a', 'b', 'c'], ['a', 'b', 'c', 'd']]
  encode_list(study, values)

  study      = ['A'];  values = [['a', 'b', '-c', 'd', 'e']]
  encode_list(study, values)

  study      = ['A', 'B']; values = [['a', 'b', 'c'], ['a', 'b', 'c']]
  encode_list(study, values)
