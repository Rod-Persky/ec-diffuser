import ConfigParser, os
import argparse, sys, textwrap, tempfile
from paramiko import SSHClient, SFTPClient, AutoAddPolicy, SSHException, Channel
from scipy import io
from time import sleep
from math import floor, fabs
import copy
import re

# Get SSH configuration from ssh.cfg (fill in ssh.cfg!)
config = ConfigParser.ConfigParser()
config.readfp(open('ssh.cfg'))

ssh_username    = config.get("ssh", "username")
ssh_password    = config.get("ssh", "password")
ssh_known_hosts = 'known_hosts'
ssh_server      = 'lyra.qut.edu.au'

sep = ['-',  # Separator for name-value pair
       '_',  # Separator for pair1_pair2
       'm']  # Notation for negative value

class QuitOnFail:
  def action(self, ssh_client, message):
    ssh_client.Disconnect()
    sys.exit("QUITTING: " + message)

class IgnoreFail:
  def action(self, ssh_client, message):
    print("IGNORING: " + message)



class SSH_Client:
  def __init__(self, _ssh_server, debug=False): 
    self.ssh_connected = False
    self.ssh_client    = SSHClient()
    self.ssh_server    = _ssh_server
    self.debug         = debug
    
    self.check_known_hosts();
    self.Connect()
    print("\nINFO    : SSH connected")


  def Connect(self):
    try:
      self.ssh_client.load_host_keys(ssh_known_hosts)
      self.ssh_client.set_missing_host_key_policy(AutoAddPolicy())
      self.ssh_client.connect(self.ssh_server,
                              username=ssh_username,
                              password=ssh_password)
                               
      self.ssh_connected = True
    except SSHException as e:
      print("Cannot connect:", e)
      
      
  def Disconnect(self):
    if self.ssh_connected:
      self.ssh_client.close()
      self.ssh_connected = False
      print("INFO    : SSH disconnected")
      
  __del__ = Disconnect
      
     
  def RunCommand(self, command, error_msg="", fail_action=IgnoreFail(), show_output=False):
    if not self.ssh_connected:
      print("SSH didn't connect")
      return
      
    if self.debug:
      print("SSH  < " + command)                                   # show the command that we're executing
      sys.stdout.flush()                                           # flush stdout

    stdin, stdout, stderr = self.ssh_client.exec_command(command)  # and then execute the command
     
    command_error = False
    output        = list()
    
    for line in stderr.readlines():            # read the stderr file and for each line
      if "tput" not in line:                   # then ignoring the tput no terminal error
        command_error = True                   # so set command_error to true
        if self.debug:
          print("SSH  >", line, end="")        # print the contents, there will only be contents on an error

    
    if command_error and 0<len(error_msg):     # If we have an error and error message
      fail_action.action(self, error_msg)      # Show this and perform the action
      
    for line in stdout.readlines():            # for each line in the output
      line = line.replace("\n", "")            # decode and remove the \n
      output.append(line)                      # add the line to the output
      if show_output or self.debug:
        print("SSH  >", line)                  # print the line
      
    return output


  def GetFolderListing(self, folder, error_msg="", fail_action=IgnoreFail()):
    if self.debug:
      print("SFTP < list " + folder)
        
    folder_sftp = folder
    if "~/" in folder:
      folder_sftp = folder.replace("~/", "")
    elif folder[0] is not "/":
      error = QuitOnFail()
      error.action(self, "Folder must be relative ~/ or absolute /home or /work/")

    server_sftp = self.ssh_client.open_sftp()
    
    try:
      folder_listing = server_sftp.listdir(folder_sftp)
      server_sftp.close()
      return folder_listing
    except Exception as e:
      print(e)
      server_sftp.close()
      fail_action.action(self, error_msg)


  def ReadRemoteFile(self, folder, filename, error_msg="", fail_action=IgnoreFail()):
    print("SFTP < read " + folder + "/" + filename)

    folder_sftp = folder
    if "~/" in folder:
      folder_sftp = folder.replace("~/", "")
    elif folder[0] is not "/":
      fail_action.action(self, "Folder must be relative ~/ or absolute /home or /work/")

    try:
      server_sftp = self.ssh_client.open_sftp()
      server_file = server_sftp.open(folder_sftp + "/" + filename)
      file_contents = server_file.readlines()
      server_file.close()
      server_sftp.close()
      return file_contents
    except Exception as e:
      print(e)
      fail_action.action(self, error_msg)
      
     
      
  def check_known_hosts(self):
    try:    # Ensure the known hosts file exists
      hosts_file = open(ssh_known_hosts, 'r')
      hosts_file.close()
    except: # Otherwise make it
      hosts_file = open(ssh_known_hosts, 'w')
      hosts_file.close()





class CFD_Case:
  def __init__(self, server, args, debug=False):

    self.server = server               # Store the server into local namespace
    self.args   = args                 # Store the args into local namespace
    self.debug  = False
    self.find_study_folder()           # We need to set the study_folder value to Study_args.study

    # Make self.study_name
    study_name = ""                    # Study name is in pattern Variable-FILE-Variable[--name].dat_
    for v_name in args.study:          # Where the pattern is repeated for each variable.
      if 0 < len(study_name):          # and we join the study name (after the first parameter)
        study_name = study_name + "_"  # with underscores
      
      study_name = (study_name         # The study name is appended with extra variables
                    + v_name           # with the Var = v_name
                    + "-FILE-"         # because we're using a file we tell the system this
                    + v_name           # by default we call the .dat file v_name.dat
                    + args.name        # although we can add a name so this becomes v_name[name].dat
                    + ".dat")          # and don't bother with other extensions just leave it .dat even if messed up
    self.study_name = study_name       # store study name in namespace

    # Unpack matlab matrix
    for i in range(0, len(args.v)):          # For each matrix we receive in --v
      args.v[i] = args.v[i].replace("]", "") # Firstly remove ]
      args.v[i] = args.v[i].replace("[", "") # and [ 
      args.v[i] = args.v[i].split(" ")       # Then split on spaces which is matlab equv of a ,
    
      for j in range(0, len(args.v[i])):     # For every value that we have received
        args.v[i][j] = float(args.v[i][j])   # we want them to be converted to a float value

    # Get number of variations
    variations = 1;                          # To calculate number of variations that will run
    for v in args.v:                         # we get the number of variations of each parameter
      variations = variations * len(v)       # and multiply them as we calculate ever combination
    if self.debug:
      print("Using", variations, "variations") # print this out, hopefully we pick up right away that there is a problem                       
    
    # Ensure study is properly setup
    self.check_case_folder()                 # Before we do anything, check that the case is properly setup
    
    # Get all variations run by the present study
    self.study_matrix, self.case_list = self.encode_list(args.study, args.v)


  def find_study_folder(self):
    """Find Study Folder
    Because we want to make it easier to specify the variables instead of
    the folder we actually need to find the folder that the study resides in
    with the format Study_variable_variable.

    We also rearrange args.study here so it can be used later"""

    server = self.server
    args   = self.args

    self.study_folder = ""                            # We will be determining the study folder
    folders = server.GetFolderListing(self.args.path) # Get a list of all Study types

    candidate_folders = list()                        # Prepare a list of candidates by
    for folder in folders:                            # looking at all the folders
      found = True                                    # and assuming an exact match
      for variable in args.study:                     # for all variables in --study
        if variable.lower() not in folder.lower():    # check that the variable is present in the folder
          found = False                               # otherwise we found a non match
          break                                       # so try another folder
          
      if found:                                       # however if our initial assumption still holds
        candidate_folders.append(folder)              # then we found a candidate folder


    for candidate in candidate_folders:               # given we will find a number of candidate folders
      if self.debug:
        print("Candidate >", candidate)               # we should show all the potential matches

      folder_len = len("Study_")                      # the exact match will contain Study_
      for variable in args.study:                     # and all the variables.
        folder_len = folder_len + len(variable)       # The folder name should contain the variable
      folder_len = folder_len + len(args.study) - 1   # and between each variable there is an _
      
      if len(candidate) == folder_len:                # we determine an exact match by the folder length
        if self.debug:
          print("Matched   >", candidate)             # so we will print this out for reference,
        self.study_folder = candidate                 # store it in study_folder,
        break                                         # and stop searching

    if len(self.study_folder) == 0:                   # if we didn't find a folder in the end then
      error = QuitOnFail()                            # then this is fatal, and we should quit
      error.action(server, "Folder for Study_" + str(args.study) + " was not found")

    # Sort variables
    correct_sort = self.study_folder.split("_")[1]       # Firstly grab the variables back from the folder name
    correct_sort = correct_sort.split("-")               # variables are denoted by a -

    # Get arrangement of variables from input that corresponds to the correct arrangement
    correct_arrangement = list([-1] * len(correct_sort)) # Allocate an array the correct length to store the arrangement with default of -1
    for i in range(0, len(correct_sort)):                # Then using the correct sorting index
      for j in range(0, len(args.study)):                # Go over each study argument
        if correct_sort[i].lower() == args.study[j].lower(): # and if they match (ignoring incorrect lower/upper case) then
          correct_arrangement[i] = j                     # store that link
      
      if correct_arrangement[i] == -1:                   # if we didn't find a match
        sys.exit("Somehow the variable in correct_sort"+ # Quit
                 " isn't present in args.study")         
    
    correct_v_arrangement = list(args.v)                        # Copy the old list
    for i in range(0, len(args.v)):                             # Then for each variation 
      correct_v_arrangement[i] = args.v[correct_arrangement[i]] # Sort to the correct location
  
    args.v     = correct_v_arrangement
    args.study = correct_sort



  def check_case_folder(self):
    args         = self.args
    study_name   = self.study_name
    study_folder = self.study_folder
    server       = self.server
    working_dir  = args.path + "/" + study_folder

    if self.debug:
      print("\nChecking the following folders exist:")
    
    server.RunCommand("cd " + working_dir,                                    # Try and cd to the folder 
                      "Study {} does not exist".format(working_dir),          # Set failure message
                      QuitOnFail())                                           # And failure action

    server.RunCommand("cd {}/_setup".format(working_dir),                     # Try and cd to the _setup folder 
                      "Study {}/_setup does not exist".format(working_dir),   # Set failure message
                      QuitOnFail())                                           # And failure action

    server.RunCommand("cd {}/_scripts".format(working_dir),                   # Try and cd to the _setup folder 
                      "Study {}/_scripts does not exist".format(working_dir), # Set failure message
                      QuitOnFail())                                           # And failure action
                      
    # Also, check that the values are similar to those that already exist
    listing = server.GetFolderListing(working_dir)
    ignore_filename = ".{}_ignore_bounds".format(study_name[:-4])
    ignore_filename = ignore_filename.replace("-", "")
    
    current_studies = list()
    for study in listing:                         
      try:                                              #
        if "-" in study:                                # the study name has a - which helps in identification
          value_pairs = study.split("_")                # each value-pair is split by an _ to indicate new value pair
          num_pairs = len(value_pairs)                  # grab the number of variable pairs
          for i in range(0, num_pairs):                 # for each study there is a
            combined = value_pairs[i].split("-")        # value pair is in format variable-value
            variable = combined[0]                      # the variable name is the first index
            value    = combined[1]                      # the variable value is the second index
            
            if len(current_studies) <= i:               # Check if this variable is stored yet
              current_studies.append(list())            # if not make some room
              
            current_studies[i].append(float(value))     # Convert the value to a float and store the value
      except:                                           # Expect there to be some extra files (aka, ignore boundary file)
        pass                                            # and just ignore it
    
    for i in range(0, len(current_studies)):            # with the variables stored, for each variable
      variable_min = min(current_studies[i])            # get the min value
      variable_max = max(current_studies[i])            # and the max value
      
      expected_min = min(self.args.v[i])                # the min value given to us as a parameter
      expected_max = max(self.args.v[i])                # and the corresponding max
       
      deviation_min = fabs((variable_min - expected_min)/variable_min) # calculate the deviation on the min
      deviation_max = fabs((variable_max - expected_max)/variable_max) # and the max
          
      if 0.5 < deviation_min or 0.5 < deviation_max:    # then if this deviation is greater than 10%
        print("Max: {}, Expected: {}".format(expected_max, variable_max))
        print("Min: {}, Expected: {}".format(expected_min, variable_min))
        
        if ignore_filename not in listing:              # and we haven't seen this warning before
          print("There may be a problem with the minimum value of the range")    # then print a warning about the deviation
          print("QUITTING: Check range or restart to ignore")                    # a quit message
          server.RunCommand("touch {}/{}".format(working_dir, ignore_filename))  # store that we have seen this error
          sys.exit()                                                             # and quit
        else:                                                                    # if we have seen this error
          print("WARNING : Ignoring boundary problem")                           # ignore it

      else:                                                                      # if there was no error
        if ignore_filename in listing:                                           # but the state was seen
          print("THANKS : Bounds were fixed!")                                   # print a nice message
          server.RunCommand("rm {}/{}".format(working_dir, ignore_filename))     # and remove the file
          


  def setup_case_folder(self):
    """ Setup Case Folder
    This function sets up the case on the CFD
    server. Performing the following steps:
    """

    args         = self.args
    study_name   = self.study_name
    study_folder = self.study_folder
    server       = self.server
    working_dir  = args.path + "/" + study_folder
    
    server.RunCommand("mkdir -p {}/studies".format(working_dir))       # Make studies directory
    
    server.RunCommand("ln -s {} {}/studies/{}".format(                 # Studies are links to the storage folder
                      working_dir, working_dir,                        # and are accessed through path/study/studies
                      study_name),                                     # the name indicates the study being run (important!!)
                      "ln fails when exists, that is ok")              # it usually complains if the study exists, don't worry
                    
    #server.RunCommand("ls -1 {}/studies/{} | grep '-'".format(         # List all studies to confirm that it now exists
    #                  working_dir, study_name))                        # just showing those that are a Variable-Value pair


  def add_variations_to_dat(self):
    args         = self.args
    study_name   = self.study_name
    study_folder = self.study_folder
    server       = self.server
    working_dir  = args.path + "/" + study_folder

    # Construct .dat files, this is when V-Vmat ordering is important    
    for i in range(0, len(args.study)):                                # For each variable we are studying
      dat_file = args.study[i] + args.name                             # the .dat is named as above variable[study].dat
      dat_file = "{}/studies/{}/{}.dat".format(working_dir, study_name, dat_file)
      server.RunCommand("touch {}".format(dat_file))                   # Ensure the dat file exists by touching it

      existing = server.RunCommand("cat " + dat_file)

      for j in range(0, len(args.v[i])):                               # For each variable
        if str(args.v[i][j]) not in existing:                          # if not in the .dat then
          server.RunCommand("echo {} >> {}".format(  # we append
                            args.v[i][j],                              # the values of the variable
                            dat_file))                                 # dat file


      
  def run_variations(self):
    args         = self.args
    study_name   = self.study_name
    study_folder = self.study_folder
    server       = self.server
    working_dir  = args.path + "/" + study_folder
    
    print("WARNING : The next command takes a very long time, do not quit the program!!")
    server.RunCommand("cd {}/studies/{} && ./parametric_launch.sh start".format(       # Go to the case directory and then use our
                      working_dir, study_name),                                        # parametric launch system to start the CFD cases.
                      "The folder should exist, as should parametric_launch.sh",       # We have created the folder earlier, so ensure the launch file exists
                      QuitOnFail())                                                    # this is fatal.
    print("PHHEWW  : It is now possible to safely quit")

  def check_finished(self, show_finished=True):
    args         = self.args
    study_name   = self.study_name
    study_folder = self.study_folder
    server       = self.server
    working_dir  = args.path + "/" + study_folder

    output = server.RunCommand("cd {}/studies/{} && ./parametric_launch.sh check".format(
                                 working_dir, study_name), show_output=False)
    
    remaining = 0
      
    for line in output:
      if "Checking" in line and show_finished:
        print("INFO >", line.strip(), end="")
      if "completed" in line:
        progress = re.findall('[0-9/]*', line)
        progress = progress[1]
        progress = progress.split("/")
        if progress[0] == progress[1]:
          if show_finished:
            print("... finished")
        else:
          if show_finished:
            print("... running")
          remaining = remaining + int(progress[1]) - int(progress[0])
       
    print("INFO    : There are " + str(remaining) + " tasks remaining")
      
    if remaining == 0:
      finished = True
    else:
      finished = False

    return finished


  def wait_results(self):
    args         = self.args
    study_name   = self.study_name
    study_folder = self.study_folder
    server       = self.server
    working_dir  = args.path + "/" + study_folder
          
    while not self.check_finished():
      print("INFO > It is now safe to quit and resume the function at any time")
      sleep(5)
      print()
        

  def parse_case(self, line, location, scores_data):
    line = line.replace("\n","") # Remove the \n in the line and then
    combined = line.split(",")   # split the line up into each value. Now we will get a list in the combined format:
                                 # casenm,param_1,param_2,param_n,value_1,value_2,value_n.
                                 
    scores = list()              # So we make a list to put the scores into
    params = list()              # and another list to put the parameters (params) into.
    middle = (len(combined)-1)/2 # We know that the values start in the middle of the param value pairs
    middle = middle + 1          # and that the middle of the param value pair is shifted by one because of the case name.

    if middle != int(middle):    # So checking this assumption middle must be a integer, but if it isn't
      error = QuitOnFail()       # we give a quit with the warning:
      error.action(self.server,   "swak_scores contains an extra or missing value or comma :" + case)
    middle = int(middle)         # finally commit to setting middle to an int now we know it is correct
    
    scores = combined[middle:]   # With a good middle the scores are the values from the middle onwards and the
    params = combined[1:middle]  # params are the values after the case name to the middle.
    casenm = combined[0]         # Obviously the case name is the first parameter as mentioned above in the pattern

    if len(scores) != len(params): # Now we expect there to be a 1:1 link between scores and parameters
      error = QuitOnFail()         # if there isn't then we give a quit with the warning:
      error.action(self.server,  "There are more scores than parameters or visa versa"
                                 + " Params: " + str(params)  # For clarity showing the params
                                 + " Scores: " + str(scores)) # and scores that we split up.
    
    # Convert values to floats (if possible)
    for i in range(0, len(scores)):  # We need to convert all the scores to floats so
      try:                           # assuming we were right above
        scores[i] = float(scores[i]) # we do that conversion.
      except Exception as e:         # However, if we were wrong then we need to stop
        error = QuitOnFail()         # so we give a quit with the warning:
        error.action(self.server,    "Unexpected string in the scores list " + str(scores))
        
    # Combine data into scores_data
    for i in range(0, len(params)):             # Now to combine the data we get a common index between param and score
      params[i] = params[i].replace("-","_")    # Replace -'s with _'s for matlab compatibility
      if params[i] not in scores_data:          # if we haven't setup this parameter in the scores_data dictionary
        scores_data[params[i]] = \
          self.create_empty_ndim_list(          # create a new ndim structure
          self.problem_dimensions)              # for the problem and
        
      self.set_ndim_element(scores_data[params[i]], location, scores[i])      
      
  def get_complete_variables(self):
    """Get Complete Variables
    The system will go through every case from the dat files
    and score them. A score is the last value in a SWAK file
    as computed by SWAK from the controlDict. The variable
    will be named exactly that which is specified in the
    controlDict

    Values are stored in a variable named after the computed
    value from the controlDict in a multidimensional array
    
    swak_var = [[v1v2_1,1, v1v2_1,2, v1v2_1,3... v1v2_1,m],
                [v1v2_2,1, v1v2_2,2, v1v2_2,3... v1v2_2,m],
                ...
                [v1v2_n,1, v1v2_n,2, v1v2_n,3... v1v2_n,m]]

    If more than 2 dimensions then extra dimensions are
    additional depths, so v1v2_m,n = [v1v2v3_1, v1v2v3...],
    which in Matlab would be accessed as swak_var(m, n, o, ...)

    The program will return the location of this file,
    which will be set to the study folder name
    """
    
    args         = self.args
    study_name   = self.study_name
    study_folder = self.study_folder
    server       = self.server
    working_dir  = args.path + "/" + study_folder

    if not self.check_finished(False):
      return

    server.RunCommand("cd " + working_dir  +   # Go to the working directory                  
                      " && rm -rf _scores" +   # Delete old scores
                      " && mkdir _scores")     # Recreate _scores directory

    for case in self.case_list:                                   # for each case that we are working with
      server.RunCommand("cd " + working_dir +                     # Go to the case directory and then use our
                        " && ./parametric_launch.sh scores " +    # parametric launch system to score the
                        "\"" + case[0] + "\"")                    # current case

    scores_data = dict()                                          # set-up the dictionary that we are going write to the matlab .mat
    scores_file = server.ReadRemoteFile(working_dir + "/_scores",  # the scoring data is stored in _scores
                                        "swak_scores",             # and the swak_scores file has all the score files nicely compiled as a csv
                                        "No cases have yet finished",
                                        QuitOnFail())
    if scores_file == None:
      sys.exit("No scores were recorded")
    
    finished = True
            
    for case in self.case_list:              # For each case we want to get the scoring (rather than each unique such that we can accept duplicate cases)
      found_score = False
      for line in scores_file:               # search for the case in the score data
        if line.split(",")[0] == case[0]:    # if the name in the score data matches the case name
          print("Match", case[0], case[1])   # indicate a match found
          self.parse_case(line,              # parse the score csv line
                          case[1],           # for the score in the matrix location
                          scores_data)       # into the scores matrix
          found_score = True
          break
          
      if not found_score:
        finished = False
        print("     ", case[0], case[1], end="")
        print(" did not succesfully complete")

    if not finished:
      sys.exit("Some cases did not finish, either crashing or running out of time")
   
    mat_location = args.path.replace('~/','') + '-' + study_name[0:-4] + '.mat'
    case_list = [value[0] for value in self.case_list]
    case_indx = [value[1] for value in self.case_list]
    scores_data['case_name']  = case_list
    scores_data['case_index'] = case_indx
    io.savemat(mat_location, scores_data)    # such that finally (once tabulating all scores) we can output this data into a matlab matrix
    
    print("<mat>" + mat_location + "</mat>")
    
    
    """ NDIM Functions
    The following functions are used to generated an n dimensional array
    create_empty_ndim_list
    set_ndim_element
    encode_list_1D
    encode_list
    """
      
  
  def create_empty_ndim_list(self, size = [1]):
    compiled_list = list([1] * size[len(size)-1])         # Initial size is the number of elements (last index)
    for i in range(len(size)-2, -1, -1):                  # Then for each parent tree
      sub_compiled  = compiled_list                       # We store the subtree (what we have up to now)
      compiled_list = list()                              # Make the parent tree
      for i in range(0, size[i]):                         # And then for the width of the tree
        compiled_list.append(copy.deepcopy(sub_compiled)) # we append i copies of the subtree
    return compiled_list
    
    
  def set_ndim_element(self, ndim_list, position, value, level = 0):
    if type(ndim_list[position[level]]) is list:          # If we are still in a parent list
      self.set_ndim_element(ndim_list[position[level]],   # then go into the sublist at the position corresponding to this level taking this level
                            position,                     # copying the position matrix
                            value,                        # value we are setting the matrix[position] to
                            level+1)                      # and an indication of how far we are in the search
    else:                                                 # If at a value
      if self.debug:
        print("Setting Matrix{} to {}".format(            # Indicate that we're setting that value
          position, value))   
      ndim_list[position[level]] = value                  # And set the value


  def encode_list_1D(self, study, values):                # 1D encode is super simple
    values = values[0]                                    # Promote
    compiled_list = list()                                # We create a list
    case_list = list()
    self.problem_dimensions = [len(values)]
    
    for i in range(0, len(values)):                    # and for each variable
      value = values[i]
      if str(value)[0] == '-':                         # ensuring the - sign is
          value = sep[2]+str(value[1:])                # replaced with a m
      case_name = study[0] + sep[0] + str(value)       # and make the case name then
      compiled_list.append(case_name)                  # we add the value to the list.
      case_list.append([case_name, [i]])               # Also we store a link between name and position

    if self.debug:
      print("List of cases handled by this run:")
      for dim in compiled_list:
        print(dim)

    return compiled_list, case_list
      
  def encode_list (self, study, values):                  # ndim is not nearly as easy
    if len(study) != len(values):                         # first check for correct sizing
      raise RuntimeError("Study and Values arrays must have same number of dimensions")

    if len(study) == 1:                                   # then if given a 1D array
      return self.encode_list_1D(study, values)

    # First we calculate the number of unique combinations
    number_combinations = 1                                  # With the Number of combinations set to
    for value in values:                                     # a value that is
      number_combinations = number_combinations * len(value) # the product of the length of all values

    # Create a ndim matrix for all the varied dimensions
    size = [len(values[i]) for i in range(0, len(values))]       # We grab the size of each dimension
    self.problem_dimensions = size                               # and save it to self.problem_dimensions then
    compiled_list = self.create_empty_ndim_list(size)            # create an empty ndim list and
    case_list = list()                                           # store the case names in a list
    
    # Create the variation matrix
    for parent_level in range(1, number_combinations+1):         # For every combination
      compiled_sublist = list([0]*len(values))                   # create a sublist that represents the tree location

      # Each variation is a unique combination of each variable though a tree
      for i in range(len(values)-1, 0, -1):                                 # For each element in the combination
        this_tree    = (parent_level-1)/float(len(values[i])) + 1           # Determine on which tree we are on
        parent_level = floor(this_tree)                                     # The parent tree is the integer part
        this_level   = round((this_tree - parent_level)*len(values[i])) + 1 # The fractional part x detail level is the current tree item
        compiled_sublist[i] = this_level -1                                 # Set this level
        compiled_sublist[i-1] = parent_level-1                              # Set parent level

      # Compile name
      case_name = ""
      
      for level in range(0, len(compiled_sublist)):          # The case name is a combination
        value = values[level][compiled_sublist[level]]
        if '-' in str(value):
          value = sep[2]+str(value)
        case_name = (case_name    + sep[1] +                 # [case_name]_
                     study[level] + sep[0] +                 # [variable name]-
                     str(values[level][compiled_sublist[level]])) # [variable value]

      self.set_ndim_element(compiled_list, compiled_sublist, case_name[1:])
      case_list.append([case_name[1:], compiled_sublist])                     # Store position
     
    return compiled_list, case_list
    
    
if __name__ == "__main__":
  parser = argparse.ArgumentParser(
    formatter_class=argparse.RawDescriptionHelpFormatter,
    description=textwrap.dedent("""Run CFD Study.
    This program launches a CFD parametric study from the
    comfort of whatever computer is available. Mainly this is
    aimed at being run from matlab.
    
    Path is the base path to the studies directory, that
    is being used. Study is then the further path into
    the specific study that is being varied.
    
    To launch a case you specify the --v[ariations] and
    corresponding variables for the --study for example:
    
    --v "[0.10 0.11 0.12 0.13]"
    --study R1
    
    The .dat file corresponds to whatever has been setup in the
    parametric launching system. The naming is abstract and
    up to you, the thisCase file only case about the ordering.
    
    If using more than 1 parameter, a 2D matrix from matlab
    can be input where each row corresponds to a variable
    which is placed in the corresponding dimensions .dat
    file. For example if varying parameters Lt and I then:
    
    --v "[0.0032 0.0033 0.0034 0.0035;0.05 0.06 0.07 0.08]"
    --study Lt I
    
    The order of the values in --v must exactly match the
    order in --study otherwise, obviously, the wrong value
    wiil be assigned to the variable.
    
    If you are looking to do different variations then you
    can specify --name which could be anything, but it must
    be unique unless you are deliberately adding more variations
    to a case. This --name will create a folder in studies,
    and will append the name before the .dat. By default the
    program will make a folder studies/Lt-FILE-Lt.dat_I-FILE-I.dat
    which is modified by the --name function. In affect:
    
    --name a
    studies/Lt-FILE-Lta.dat_I-FILE-Ia.dat

    --v will launch the cases, however will not return any
    results, this is so you can programmatically launch a whole
    batch.
    
    --get If you only want to retrieve completed results then use
    in specified with --v. It will not launch any that do not exit.

    --wait can be used if you want to wait, specifying --wait will
    cause all launch methods to wait and return results, so this
    works if launching just one set of cases with --v and you
    don't want to specify --get --wait""")
  )
                       
  parser.add_argument('--path', required=True,
                      help='Location of the Study folders')

  parser.add_argument('--study', required=True, nargs="*",
                      help='Variables to be studied in a list like variable_1 variable_2')
                                                                 
  parser.add_argument('--name', default='',
                      help='Launch a new named variation')
                      
  parser.add_argument('--wait', default='N', choices=['Y', 'N'],
                      help='Wait for cases to finish and return results for cases specified in --r, --v')
                      
  parser.add_argument('--get', default='N', choices=['Y', 'N'],
                      help='Will return map with variables for the specified cases (--v)')

  parser.add_argument('--debug', default='N', choices=['Y', 'N'],
                      help='Show heaps of text to help debugging')
                      
  parser.add_argument('--v',
                      help='Variations of parameter, will not retun the results unless --wait is given')
                      
  args = parser.parse_args()

  args_dict = args.__dict__
  
  if args.debug == 'Y':
    for key in args_dict:
      print("{:5}: {}".format(key, args_dict[key]))

  args.v = args.v.split(";")                  # Matlab rows are separated by a ;

  if args.debug == 'Y':
    args.debug == True
  else:
    args.debug == False

  if len(args.v) is not len(args.study):             # thses should be the same, if not then quit
    print("\nGot", len(args.v), "--v[ariation] for", # Indicate the number of variables that we received
          len(args.study), "--study")                # and the number of dat files that have been specified
    sys.exit("Ensure that the number of .dat files corresponds with the number of variables")

  lyra     = SSH_Client(ssh_server) # Connect to the ssh server
  cfd_case = CFD_Case(lyra, args)               # cfd case contains all the functions we need to interact with the cfd launcher

  # Must always run the intended cases otherwise results may be missing
  cfd_case.setup_case_folder()                #   set-up the case folder
  cfd_case.add_variations_to_dat()            #   add variations to the dat file
  cfd_case.run_variations()                   #   then run the cases

  if args.wait == "Y":                        # If we launched and are willing to wait for results, we enter this routine
    cfd_case.wait_results()

  if args.get == "Y" or args.wait == "Y":     # If we specified get or wait then we give results back   
    cfd_case.get_complete_variables()
    
  lyra.Disconnect()