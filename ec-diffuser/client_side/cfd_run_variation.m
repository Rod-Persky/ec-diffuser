function matfile = cfd_run_variation(lyra_workdir, variables, variation_mat, sname, wait)
    % This function wall run all variations and then return all the
    % results. It is safe to quit and resume this function once
    % it tells you it is safe, if you quit before it is safe then you'll
    % have to delete all the cases manually (you're on you own if you do
    % that)

    % variables is a space seperated string like 'varable 1 variable2' 
    % variation_mat is format [value_1, value_2, ... value_n; ...]
    % result is in format [value_1, value_2, ... value_n]
    
    % The order of variables must match the order given in the variables
    % parameter
    
    % Variables for study (directories and files on lyra)
    lyra_workdir   = strcat('~/', lyra_workdir);  % Location of working directory
    variations     = mat2str(variation_mat);      % Variations to add to cfd_param_dat
    
    % Local system program locations and 
    python3_path    = 'C:\python33\python.exe'; % Location of python 3.3
    python_launcher = 'launch.py';
    command         = strjoin({python3_path, python_launcher, ...
                              '--path', strcat('"',lyra_workdir,'"'), ...
                              '--study', variables, ...
                              '--name', sname, ...
                              '--get Y', ...
                              '--debug N', ...
                              '--wait ', wait, ...
                              '--v', strcat('"',variations,'"'), ...
                              }, ' ');

    fprintf('Running %s\n', command);    
    [~,cmdout] = system(command, '-echo');
    matfile = cmdout(strfind(cmdout, '<mat>')+5:strfind(cmdout, '</mat>')-1);
    
    
    